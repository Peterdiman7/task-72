import { useState } from "react";

const Tags = () => {
  const [MyTags, setMyTags] = useState(['boomdotdev', 'task', 'tags', 'react']);

    console.log(MyTags);
    return(
        <div>
            {MyTags.map((x) => {
                return(<p className="tag">{"#" + x}</p>);
            })}
        </div>
    );
}

export default Tags;